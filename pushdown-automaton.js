class Output{
    constructor(){}
    displayState(state){
        document.getElementsByClassName(active)[0].classList.toggleClass('active');
        document.getElementById(state).classList.toggleClass('active');
    }
    sendOutput(display,msg){
        document.getElementById(display).innerHTML = msg;
    }
}
class Actions{
    constructor (){ //['q0','wfi','ga','ba','sr','rga','rba','qf']
        this.index = 0;
        this.words = [['example', 'przykład'], ['yes', 'tak']]; //words
    }


    //CONDITIONS//
    words_finished(){ return (this.index >= this.words.length)}
    words_not_finished(){ return (this.index < this.words.length)}
    buffer_empty(){ return (this.buffer === null)}
    buffer_full(){ return (this.buffer !== null)}
    //CONDITIONS//


    start(){ 
        output.sendOutput('word', this.words[this.index][0]);
        return 'waiting';
    }

    compare(){
        if (this.buffer === this.words[this.index][1]) {
            this.buffer = null;
            return 'good_answer';
        }
        else {
            this.buffer = null;
            return 'bad_answer';
        }
    }

    goodFeedback(){
        output.sendOutput('points', ++this.points);
        output.sendOutput('feedback', 'Good job!');
        return 'feedback';
    }
    badFeedback(){
        output.sendOutput('feedback', 'Wrong answer...');
        return 'feedback';
    }

    showNext(){ 
        output.sendOutput('feedback', '');
        if (++this.index < this.words.length){
            output.sendOutput('word',this.words[this.index][0]);
            return 'waiting';
        }
        else return 'qf';
     }

    wait(){ return this.current_state }

    getInput(input){
        this.buffer = input;
        return this.current_state;
    }

    end(){ clearInterval(this.impulse) }
}

class Automaton extends Actions{

    constructor (output){ //['q0','wfi','ga','ba','sr','rga','rba','qf']
        super();
        this.buffer = null; //user input
        this.output = output; //view for automaton
        this.points = 0;
        this.current_state = 'q0'; //obecny stan automatu - ustawiony na stan początkowy 'q0'
        this.impulse = setInterval(this.transition.bind(this), 3000);
    }
   

    transition(input = null){ //sprawdza stan, sprawdza dostępne przejścia, ewaluuje, wykonuje przejście, zmienia stan
        console.log(this.current_state);
        if (input !== null) this.current_state = this.getInput(input); //odblokoway dla każdego stanu

        else { //przejście wywołane wewnętrznym impulsem
            //var possible_actions = this.states[this.current_state];
            switch(this.current_state){
                case 'q0':
                    if (this.words_not_finished()) this.current_state = this.start();
                    else if (this.words_finished()) this.current_state = this.end();
                    else console.log('no action aviable');
                    break;
                case 'waiting':
                    if (this.buffer_empty()) this.current_state = this.wait();
                    else if (this.buffer_full()) this.current_state = this.compare();
                    else console.log('no action aviable');
                    break;
                case 'good_answer':
                    this.current_state = this.goodFeedback();
                    break;
                case 'bad_answer':
                    this.current_state = this.badFeedback();
                    break;
                case 'feedback':
                    if (this.words_not_finished()) this.current_state = this.showNext();
                    else if (this.words_finished()) this.current_state = this.end();
                    else console.log('no action aviable');
                    break;
                case 'qf':
                    this.end();
                    break;
                default:
                    console.log('transition failed');
                    break;
            }
            //SHOW STATE CHANGE
        }
    }

}
